#未风前端规范标准实践
***

##常用UI库

- [Bootstrap](http://v3.bootcss.com/css)
- [Semantic UI](http://www.semantic-ui-cn.com/)
- [purecss](https://purecss.io/) |轻量级 -> !值得一看
- [Frozen UI移动UI](http://frozenui.github.io/)
- [We UI 微信出品](https://github.com/weui/weui) |微信
- [Mint UI](http://mint-ui.github.io/#!/zh-cn) |美团|Vue2
- [Vux](https://vuxjs.gitbooks.io/vux/content/) |Vue2|微信
- [Material Design](http://materializecss.com/about.html)
- [MSUI](http://m.sui.taobao.org/about/) |阿里巴巴
- [Mobile Angular UI](http://mobileangularui.com/) |Boostrap|Angularjs
- [ngSemantic](https://ng-semantic.herokuapp.com/) |Semantic|Angularjs
- [ng2-bootstrap](http://valor-software.com/ng2-bootstrap/) |Boostrap|Angularjs
- [更多AngularUI资源](http://angular2.axuer.com/resources/)


##黄金定律
###永远遵循同一套编码规范 -- 可以是这里列出的，也可以是你自己总结的。
请参考阅读
1. [基本编码规范 by @mdo](http://codeguide.bootcss.com/#css-prefixed-properties)
2. [编写可维护的CSS](https://segmentfault.com/a/1190000000388784)
3. [编写可维护的Javascript](http://note.youdao.com/share/?id=eab0ce8356e8b4edf69ed1b76d67fd8c&type=note#/)
4. [腾讯Web知识库](https://github.com/AlloyTeam/Mars)
5. [谷歌 HTML/CSS 规范](https://segmentfault.com/a/1190000007023192)
6. [简练清晰的HTML代码](https://zhuanlan.zhihu.com/p/25842993?utm_source=wechat_session&utm_medium=social)

##目录说明
1. `html5-boilerplate` 目录下是[html5-boilerplate](http://www.w3cplus.com/html5/html5-boilerplate.html)的模板
2. `Bootstrap3` 目录下是[Bootstrap提供用于生产环境的一套模板](http://v3.bootcss.com/getting-started/#download)，更多关于Bootstrap的开发工具请参考[Bootstrap中文网](http://www.bootcss.com/)
3. `template` 目录下是一些常用的HTML模板，欢迎补充


##图片图标资源
1. 矢量图标 [阿里出品](http://www.iconfont.cn/plus)
2. [免费高清图片](https://pixabay.com/)
3. 矢量图标 [icomoon](https://icomoon.io/)
4. [Font Awesome](http://fontawesome.dashgame.com/)

##设计向
###注意，以下站点可能被墙，自寻解决
1. 移动UI设计网站[material](https://material.uplabs.com/)
2. 设计风[voicer](http://www.voicer.me/)

##学习资源
1. [JavaScript](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/A_re-introduction_to_JavaScript)

##CDN加速服务
- [BootCDN](http://www.bootcdn.cn/)
- [CDNjs](https://cdnjs.com/)


##ES6概念深入
- [知乎专栏](https://zhuanlan.zhihu.com/study-fe)


##BAT相关
- [腾讯前端](http://tgideas.qq.com/)
- [腾讯用户设计](https://isux.tencent.com/)